# ------------------------------------------------------------------------------
# MW & recall precision analyse 0

# NOTE: we share this code for reasons of transparency, but 
# we did not provide the datafile needed for this script because it entailed
# private data of our participants. please start with 'analyse 1' and the provided
# datafiles 'MW_memory_data.csv' and 'MW_demo_quest.csv'.

# does:
# - read in raw datafile from lab.js. (after converting SQL.lite to .csv) 
# - anonymize and prepare
# 
# 2020-06-09 
# 2021-06-02
# Rebecca Ovalle-Fresa, r.ovallefresa@gmail.com

# R version 4.0.5 (2021-03-31)
# Platform: x86_64-pc-linux-gnu (64-bit)
# Running under: Manjaro Linux

# more detailed session info incl package versions is provided at the end of the script

# -------------------------------------------------------- clear and inform ----
#
# ******************************************************************************

# clear console
cat('\014')


# clear workspace
rm(list=ls())


# clear plotspace (if it is not already empty)
if (!is.null(dev.list())) {
  dev.off(dev.list()['RStudioGD'])
}


# decide to save plots
saveplots = 1

# --------------------------------------------------------- open libraries -----
#
# ******************************************************************************

library(tidyverse)


# check R version
sessionInfo()


# negate value matching operator %in%
'%ni%' = Negate('%in%')

# --------------------------------------------------------- define datasets ----
#
# ******************************************************************************

# ---- participants to exclude -------------------------------------------------

# 2: started task a second time because of technical problems
# 2: no complete dasaset (only demographic questionnaire)

exc_pxs <- c(999)
exc_init <- c('test')

# -------------------------------------------------------------- directories ---
#
# ******************************************************************************
# 

# set working directory
dir  <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
dir  <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

# subdirectorie(s) for data input files
dir_i <- paste0(substring(dir, 1, nchar(dir)-7), '/inR/raw/') # data 

# create subdirectories per condition for figures and data output files
dir_o1     <- paste0(substring(dir, 1, nchar(dir)-7), '/outR/') # output
dir_o     <- paste0(substring(dir, 1, nchar(dir)-7), '/outR/analyse_0/') # output

dir_f <- paste0(substring(dir, 1, nchar(dir)-7), '/figures/') # output figures

# create subdirectories if they do not exist
dir.create(dir_o, showWarnings = FALSE)
dir.create(dir_o1, showWarnings = FALSE)

dir.create(dir_f, showWarnings = FALSE)

# --------------------------------------------------------------- read data ----
#
# ******************************************************************************

# raw data file (includes colour, position and orientation)
paths   <- list.files(dir_i, pattern = '*.csv', full.names = TRUE)
files   <- tibble(filename = basename(paths))

raw0 <- tibble(file = basename(paths)) %>%
  extract(file, 'filename', '([A-Z]{2}-[A-Za-z0-9]{3})', remove = FALSE) %>%
  mutate(Data = lapply(paths, read.csv, stringsAsFactors = FALSE)) %>%
  unnest(Data) %>%
  janitor::clean_names() %>%
  select(-filename)


raw0 <- raw0 %>% 
  group_by(subj_n) %>%
  fill(initials, .direction = "downup") %>%
  arrange(subj_n, timestamp) %>%
  fill(block_counter, .direction = "downup") %>%
  mutate(condition = case_when(str_detect(file, 'pool') ~ 'autonomous',
                               !str_detect(file, 'pool') ~ 'phone'))


# ---------------------------------------------------- exclude participants ----
#
# ******************************************************************************
raw <- raw0  %>%
  filter(subj_n %ni% exc_pxs, initials %ni% exc_init, !is.na(subj_n)) 

# ------------------------------------------------------------ prepare data ----
#
# ******************************************************************************

# create new, anonymous participant id

new_ids <- as_tibble(array(1:length(unique(raw$subj_n)))) %>%
  cbind(subj_n = unique(raw$subj_n)) %>%
  mutate(new_subj_n = value + 100) %>%
  select(-value)

raw <- raw %>%
  full_join(new_ids) %>%
  mutate(subj_n = new_subj_n) %>%
  select(-new_subj_n)

# use all english labels and create variable for english version
raw <- raw %>%
  mutate(exp_language = case_when(gender == 'female' | gender == 'male' ~ 'english',
                        gender == 'weiblich' | gender == 'männlich' ~ 'german'),
         gender = case_when(gender == 'weiblich'  ~ 'female',
                            gender == 'männlich' ~ 'male',
                            TRUE ~ as.character(gender)),
         hand =  case_when(hand == 'links'  ~ 'left',
                           hand == 'rechts' ~ 'right',
                           TRUE ~ as.character(hand)),
         education =  tolower(education),
         education =  case_when(education == 'doctorate / phd' | education == 'doktorat'  ~ 'doctorate',
                           education == 'matura' ~ 'highschool',
                           education == 'sekundarschule' ~ 'secondaryschool',
                           education == 'lehre' ~ 'apprehenticeship',
                           TRUE ~ as.character(education))
         )

# ---- consent and screen infos
tmp_c <- raw %>%
  filter(sender == 'Consent') %>%
  select(subj_n, initials, timestamp, consent, meta_device_pixel_ratio, meta_screen_height, meta_screen_width)

# ---- demographic data
tmp_d <- raw %>%
  filter(sender == 'Infos') %>%
  select(timestamp, exp_language, condition, subj_n, initials, day, month, year, gender, hand, education) %>%
  mutate(dateB = as.Date(paste0(year, '-', month, '-', day)),
         dateT = as.Date(substring(timestamp, 1, 10))) %>%
  mutate(age = as.numeric(dateT - dateB) / 365) %>%
  select(-dateT, -dateB)

# ---- questions
demo_q1 <- raw %>%
  filter(sender == 'Questions') %>%
  select(subj_n, initials, starts_with('freq_'))
  
# ---- questions SDMWS
demo_q2 <- raw %>%
  filter(sender == 'SDMWS') %>%
  select(subj_n, initials, starts_with('dmw'), starts_with('smw'))

demo_quest <- left_join(tmp_c, tmp_d, by = c('subj_n', 'initials')) %>%
  left_join(demo_q1, by = c('subj_n', 'initials')) %>%
  left_join(demo_q2, by = c('subj_n', 'initials'))

# remove duplicates (= participants clicking too early and twice on the link)
demo_quest_dupl <- demo_quest[duplicated(demo_quest$subj_n), ]
demo_quest <- demo_quest[!duplicated(demo_quest$subj_n), ]


# create age & education level variables
# https://ec.europa.eu/eurostat/statistics-explained/index.php/International
# _Standard_Classification_of_Education_(ISCED)#Implementation_of_ISCED_2011_.28levels_of_education.29

# ISCED 0: Early childhood education (‘less than primary’ for educational attainment)
# ISCED 1: Primary education
# ISCED 2: Lower secondary education
# ISCED 3: Upper secondary education
# ISCED 4: Post-secondary non-tertiary education
# ISCED 5: Short-cycle tertiary education
# ISCED 6: Bachelor’s or equivalent level
# ISCED 7: Master’s or equivalent level
# ISCED 8: Doctoral or equivalent level

demo_quest <- demo_quest %>%
  mutate(education_level = case_when( education == 'primary' ~ 1,
                                      education == 'real' ~2,
                                      education == 'secondaryschool' ~2,
                                      education == 'highschool' ~ 3,
                                      education == 'apprehenticeship' ~ 4,
                                      education == 'meister' ~5,
                                      education == 'bachelor' ~6,
                                      education == 'master' ~7,
                                      education == 'doctorate' ~8)) 


demo_quest <- demo_quest %>%
  select(subj_n:education, education_level, age:smw4) %>%
  select(-timestamp.x, -timestamp.y, -day, -month, -year, -initials) 

srry <- demo_quest %>%
  group_by(condition) %>%
  summarise(n())

# ------------------------------------------------------------------------------

# memory data

# prepare encoding data
tmp <- raw %>%
  filter(sender == "Image_Encoding") %>%
  select(timestamp, subj_n, sender_id, block_counter, sender, duration, img_stim, 
         img_pos_x, img_pos_y) %>%
  mutate(img_pos_x_e = img_pos_x, img_pos_y_e = img_pos_y, duration_image = duration) %>%
  select(-img_pos_x, -img_pos_y)

tmp2 <- raw %>%
  filter(sender == "Probe", !is.na(response)) %>%
  select(timestamp, sender_id, subj_n, block_counter, sender, duration, starts_with('img'), 
         response, -img_stim, -img_pos_x, -img_pos_y) %>%
  pivot_longer(img_list_a:img_list_j, names_to = 'img_list_2', values_to = 'img_stim_2') %>%
  mutate(img_list_3 = toupper(paste0(substring(img_list_2, nchar(img_list_2), nchar(img_list_2)))))%>%
  filter(img_list_3 == img_list) %>%
  mutate(img_stim = img_stim_2) %>%
  select(timestamp, block_counter, sender, duration, response, img_list, img_stim)

tmp3 <- raw %>% 
  filter(sender == "Probe_Exact", !is.na(response)) %>%
  select(timestamp, sender_id, subj_n, block_counter, sender, duration, starts_with('img'), 
         response, -img_stim, -img_pos_x, -img_pos_y) %>%
  pivot_longer(img_list_a:img_list_j, names_to = 'img_list_2', values_to = 'img_stim_2') %>%
  mutate(img_list_3 = toupper(paste0(substring(img_list_2, nchar(img_list_2), nchar(img_list_2)))))%>%
  filter(img_list_3 == img_list) %>%
  mutate(img_stim = img_stim_2, response_exact = as.double(response), duration_response_exact = duration) %>%
  select(block_counter, duration_response_exact, response_exact, img_list, img_stim)


data_enc <-  left_join(tmp2, tmp3, by = c('subj_n', 'block_counter', 'img_list', 'img_stim'))

data_enc <- full_join(tmp, data_enc, by = c('subj_n', 'img_stim')) %>% 
  select(-block_counter.y)


# fill response to probe for all previous presented stimuli. do not fill 'img_list,
# to identify probe with this
data_enc <- data_enc %>%
  group_by(subj_n, block_counter.x) %>%
  arrange(subj_n, timestamp.x) %>%
  fill(response, .direction = 'up') %>%
  fill(timestamp.y, .direction = 'up') %>%
  fill(duration.y, .direction = 'up') %>%
  fill(block_counter.x, .direction = 'up')%>%
  fill(sender.y, .direction = 'up') %>%
  fill(duration_response_exact, .direction = 'up') %>%
  fill(response_exact, .direction = 'up') 


data_rec <- raw %>%
  filter(sender == 'Image_Recall') %>%
  mutate(img_pos_x_r = arranged_img_pos_x, img_pos_y_r = arranged_img_pos_y) %>%
  select(subj_n, sender, sender_id, condition, img_stim, timestamp, duration, img_pos_x_r, img_pos_y_r)


# probe_pos_back: 0 = last trial before probe
data_wide <- right_join(data_enc, data_rec, by = c('subj_n', 'img_stim'), suffix = c('_e', '_r')) %>%
  mutate(timestamp_e = timestamp.x, 
         timestamp_p = timestamp.y,
         timestamp_r = timestamp,
         duration_e = duration.x, 
         duration_p = duration.y, 
         duration_r = duration) %>%
  fill(response, .direction = 'up')  %>%
  fill(response_exact, .direction = 'up') %>%
  select(-starts_with('timestamp')) %>%
  mutate(trial_e = substr(sender_id_e, nchar(sender_id_e)-3, nchar(sender_id_e)-2), 
         trial_r = substr(sender_id_r, nchar(sender_id_r)-3, nchar(sender_id_r)-2),
         trial_e = sub('_', '0', trial_e),
         trial_r = sub('_', '0', trial_r),
         probe_trial = case_when(!is.na(img_list) ~ trial_e)) %>%
  fill(probe_trial, .direction = 'up') %>%
  mutate(probe_pos_back = as.numeric(probe_trial)- as.numeric(trial_e)) %>%
  rename(block = block_counter.x)

# ----------------------------------------------------- prepare memory data ----
#
# ******************************************************************************

# calculate angle during encoding
data_wide$radius_e  <- sqrt(data_wide$img_pos_x_e*data_wide$img_pos_x_e + data_wide$img_pos_y_e *data_wide$img_pos_y_e);
data_wide$angle_e = acos(data_wide$img_pos_x_e/data_wide$radius_e)/pi * 180

# correct angle depending on quadrant
data_wide[data_wide$img_pos_x_e == 0 & data_wide$img_pos_y_e > 0, ]$angle_e <- 90
data_wide[data_wide$img_pos_x_e == 0 & data_wide$img_pos_y_e < 0, ]$angle_e <- 270
data_wide[data_wide$img_pos_y_e == 0 & data_wide$img_pos_x_e > 0, ]$angle_e <- 0
data_wide[data_wide$img_pos_y_e == 0 & data_wide$img_pos_x_e < 0, ]$angle_e <- 180


data_wide[data_wide$img_pos_x_e < 0 & data_wide$img_pos_y_e < 0, ]$angle_e <- 360 - data_wide[data_wide$img_pos_x_e < 0 & data_wide$img_pos_y_e < 0, ]$angle_e
data_wide[data_wide$img_pos_x_e > 0 & data_wide$img_pos_y_e < 0, ]$angle_e <- 360 - data_wide[data_wide$img_pos_x_e > 0 & data_wide$img_pos_y_e < 0, ]$angle_e

min(data_wide$angle_e, na.rm = TRUE)
max(data_wide$angle_e, na.rm = TRUE)

# calculate angle during recall
data_wide$radius_r  <- sqrt(data_wide$img_pos_x_r*data_wide$img_pos_x_r + data_wide$img_pos_y_r *data_wide$img_pos_y_r);
data_wide$angle_r = acos(data_wide$img_pos_x_r/data_wide$radius_r)/pi * 180

# correct angle depending on quadrant
data_wide[data_wide$img_pos_x_r == 0 & data_wide$img_pos_y_r > 0, ]$angle_r <- 90
data_wide[data_wide$img_pos_x_r == 0 & data_wide$img_pos_y_r < 0, ]$angle_r <- 270
data_wide[data_wide$img_pos_y_r == 0 & data_wide$img_pos_x_r > 0, ]$angle_r <- 0
data_wide[data_wide$img_pos_y_r == 0 & data_wide$img_pos_x_r < 0, ]$angle_r <- 180


data_wide[data_wide$img_pos_x_r < 0 & data_wide$img_pos_y_r < 0, ]$angle_r <- 360 - data_wide[data_wide$img_pos_x_r < 0 & data_wide$img_pos_y_r < 0, ]$angle_r
data_wide[data_wide$img_pos_x_r > 0 & data_wide$img_pos_y_r < 0, ]$angle_r <- 360 - data_wide[data_wide$img_pos_x_r > 0 & data_wide$img_pos_y_r < 0, ]$angle_r

min(data_wide$angle_r, na.rm = TRUE)
max(data_wide$angle_r, na.rm = TRUE)

# position deviation: calculate and rename
data_wide$position_dev <- (((data_wide$angle_r - data_wide$angle_e + 180)%%360 ) - 180)
min(data_wide$position_dev, na.rm =TRUE)
max(data_wide$position_dev, na.rm = TRUE)

data_wide <- data_wide %>%
  rename(img_pos_angle_r = angle_r, img_pos_angle_e = angle_e, recall_error = position_dev) %>%
  select(subj_n, condition, block, img_stim, trial_e, sender_id_e, duration_image,
         img_pos_x_e, img_pos_y_e, img_pos_angle_e, probe_trial, probe_pos_back, response, duration_p, response_exact, duration_response_exact,
         trial_r, sender_id_r, img_pos_x_r, img_pos_y_r, img_pos_angle_e, img_pos_angle_r, recall_error,
          duration_r)
  
# ------------------------------------------------------------------------------

# save datafiles
write_csv(data_wide, paste0(dir_o, 'MW_memory_data', '.csv'), 
          na = "NA", append = FALSE, col_names = TRUE,
          quote_escape = "double")

write_csv(demo_quest, paste0(dir_o, 'MW_demo_quest', '.csv'),
          na = "NA", append = FALSE, col_names = TRUE,
          quote_escape = "double")

# ---------------------------------- END ---------------------------------------
# ------------------------------------------------------------------------------


# ---- session info 04-06-2021--------------------------------------------------

# R version 4.0.5 (2021-03-31)
# Platform: x86_64-pc-linux-gnu (64-bit)
# Running under: Manjaro Linux
# 
# Matrix products: default
# BLAS:   /usr/lib/libopenblasp-r0.3.14.so
# LAPACK: /usr/lib/liblapack.so.3.9.1
# 
# locale:
#   [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C               LC_TIME=de_CH.UTF-8       
# [4] LC_COLLATE=en_US.UTF-8     LC_MONETARY=de_CH.UTF-8    LC_MESSAGES=en_US.UTF-8   
# [7] LC_PAPER=de_CH.UTF-8       LC_NAME=C                  LC_ADDRESS=C              
# [10] LC_TELEPHONE=C             LC_MEASUREMENT=de_CH.UTF-8 LC_IDENTIFICATION=C       
# 
# attached base packages:
#   [1] stats     graphics  grDevices utils     datasets  methods   base     
# 
# other attached packages:
#   [1] forcats_0.5.0   stringr_1.4.0   dplyr_1.0.2     purrr_0.3.4     readr_1.3.1     tidyr_1.1.0    
# [7] tibble_3.0.3    ggplot2_3.3.2   tidyverse_1.3.0
# 
# loaded via a namespace (and not attached):
#   [1] Rcpp_1.0.5       cellranger_1.1.0 pillar_1.4.6     compiler_4.0.5   dbplyr_1.4.4    
# [6] tools_4.0.5      jsonlite_1.7.1   lubridate_1.7.9  lifecycle_0.2.0  nlme_3.1-152    
# [11] gtable_0.3.0     lattice_0.20-41  pkgconfig_2.0.3  rlang_0.4.10     reprex_0.3.0    
# [16] cli_2.0.2        DBI_1.1.0        rstudioapi_0.11  haven_2.3.1      withr_2.2.0     
# [21] xml2_1.3.2       httr_1.4.1       janitor_2.0.1    fs_1.4.1         hms_0.5.3       
# [26] generics_0.0.2   vctrs_0.3.4      grid_4.0.5       tidyselect_1.1.0 snakecase_0.11.0
# [31] glue_1.4.2       R6_2.4.1         fansi_0.4.1      readxl_1.3.1     modelr_0.1.8    
# [36] blob_1.2.1       magrittr_1.5     backports_1.1.10 scales_1.1.1     ellipsis_0.3.1  
# [41] rvest_0.3.5      assertthat_0.2.1 colorspace_1.4-1 stringi_1.5.3    munsell_0.5.0   
# [46] broom_0.5.6      crayon_1.3.4   
