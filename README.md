# MW and memory precision, study 1: analyses scripts 

the scripts were run on a folder structure with the following folders:
- progsR: containing the R scripts
- inR: containing the datasets 'MW_memory_data.csv' and 'MW_demo_quest.csv'
- outR: here, newly created datafiles are saved during the analyses, in subfolders according to the number in the script name. subsequent analyses read the new datafiles from this folders

detailed information about used packages and package versions can be found at the top of each script.

### MW_position_analyse0_anonymize_prepare.R ###

NOTE: this script is shared for reasons of transparency, as it shows the preprocessing steps. the original datafile on which this script was run is not provided, because it contains private data of the participants.

INPUT: NA

PROCESSING: 

- anonymization (create new participant numbers, delete information about OS and browser, birthday, date, etc.)
- filter out two participants with incomplete data and datasets from two participants who started the task twice
- calculate positions in degrees from x- and y- coordinates
- create data structure with one line per trial / stimulus for encoding and recall phase

OUTPUT: 'MW_memory_data.csv' and 'MW_demo_quest.csv (provided in inR)

 
### MW_position_analyse1_outlier.R

INPUT: MW_memory_data.csv and MW_demo_quest.csv (provided in inR)

LIBRARIES: tidyverse, rstudioapi, CircStats

PROCESSING:

- read in MW_memory_data.csv and MW_memory_data.csv
- plot error deviation distributions (histograms) per participant and conditions
- test against uniform distribution per participant and conditions
- exclude participants with uniform recall error distributions, reflecting pure guessing
- exclude participants with >80% inconsistent responses (binary vs. cont thought probes)

OUTPUT: 

data (folder OutR - analyse_1):
- *MW_memory_cleaned.csv*: memory data without outlier
- *MW_outlier_overall.csv*: memory data for outlier (guessing)
- *MW_outlier_nonfit.csv*: memory data for outlier (inconsistency)
- *MW_demo_quest_cleaned.csv*: demography and questionnaire data without outlier

figures (folder figures):
- *MW_mem_hist_pp.pdf*: histograms with recall error distributions per participant
- *MW_mem_hist_pc.pdf*: histograms with recall error distributions per assistance condition (phone-assisted vs. not-assisted) and MW response (off-task vs. on-task)


### MW_position_analyse2_aggregation_descriptives.R

INPUT: MW_memory_cleaned.csv and MW_demo_quest_cleaned.csv (created in analyse1)

LIBRARIES: tidyverse, rstudioapi, schoRsch

PROCESSING:

- read in memory and questionnaire data
- sample description
- aggregate SMWDMW scales
- aggregate memory data
- descriptive statistics across all participants (memory and MW)
- merge memory and questionnaire data
- save two datafiles (a) one row per participant and trial, b) aggregated data per participant and MW response (on-task vs. off-task)

OUTPUT: 

- *MW_per_trial.csv*: datafile with one row per trial
- *MW_aggregated_modelfree.csv*': datafile aggregated per participant and MW response (on-task vs. off-task)


### MW_position_analyse3_glmm_continous.R

LIBRARIES: tidyverse, rstudioapi, GGally, lmerTest

INPUT: *MW_per_trial.csv*

PROCESSING:
- correlational analyses and scatterplots
- regression analyses

OUTPUT: 
- *MW_correlations.doc*: correlational table with recall error and MW measures
- *MW_scatterplots.pdf*
